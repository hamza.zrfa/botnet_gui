use async_std::io::ReadExt;
use async_std::net::TcpStream;
use async_std::sync::{Mutex, Arc};
use async_std::{task, io, net::TcpListener};
use async_std::stream::StreamExt;
use uuid::{Uuid};

extern crate sysinfo;
//extern crate local_ip;

#[async_std::main]
async fn main() -> io::Result<()>{

    /*******************CONFIGURATION*******************/

    //Set the IP address of the server here
    let ip = "127.0.0.1";
    //Set the port of the server here
    let socket = "8080";

    /**********************PROGRAM**********************/
    
    let lock_vec : Arc<Mutex<u8>> = Arc::new(Mutex::new(0));

    let connections: Vec<TcpStream> = vec![];
    let connections = Arc::new(Mutex::new(connections));
    
    let listener = TcpListener::bind(format!("{}:{}", ip, socket)).await?;
    let mut incoming = listener.incoming();

    while let Some(stream) = incoming.next().await {
        let stream = stream?;
        let connections = connections.clone();
        let mut write_permission = connections.lock().await;
        write_permission.push(stream.clone());
        drop(write_permission);
        let lock_vec_ = Arc::clone(&lock_vec);
        task::spawn(on_connection(stream, connections, lock_vec_));
    }

    Ok(())
}

async fn on_connection(mut stream: TcpStream, connections: Arc<Mutex<Vec<TcpStream>>>, lock_vec_ : Arc<Mutex<u8>>) -> io::Result<()>{
    let addr = stream.peer_addr()?;

    let mut buffer = [0u8; 1024];
    loop {
        let len = stream.read(&mut buffer).await?;
        if len > 0 {
            let message_received = String::from_utf8_lossy(&buffer[..len]);
            //println!("{}", &message_received);

            //The message
            let to_split : Vec<&str> = message_received.as_ref().split(":").collect();

            //the code number
            let to_match = *to_split.get(0).unwrap();

            //the data
            let data = *to_split.get(1).unwrap();

            let lock_vec = Arc::clone(&lock_vec_);

            //let mut uuid : Uuid = Uuid::new_v4();
            match to_match{

                "00001" => {
                    println!("the master is connected !");
                },

                "00002" => {
                    println!("ping {}", data);
                },

                _ => println!("code number not recognized")
            }
        } else {
            println!("Disconnected: {}", stream.peer_addr()?);
            let mut connections_guard = connections.lock().await;
            
            let client_index = connections_guard.iter().position(|x| 
                (*x).peer_addr().unwrap() == stream.peer_addr().unwrap()).unwrap();
            connections_guard.remove(client_index);

            break
        }
    }

    Ok(())
    
}